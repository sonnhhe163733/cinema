#pragma checksum "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "bf00fa5f7d75f511d0ebd68bbde9bc8889292278"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Search), @"mvc.1.0.view", @"/Views/Home/Search.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\_ViewImports.cshtml"
using Cinema;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\_ViewImports.cshtml"
using Cinema.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bf00fa5f7d75f511d0ebd68bbde9bc8889292278", @"/Views/Home/Search.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5a7cf688da408230b95a46348cbff3a5802076f5", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Home_Search : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/home.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "bf00fa5f7d75f511d0ebd68bbde9bc88892922784044", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n\n<div class=\"container\">\n    <div class=\"container-left\">\n        <h1 class=\"category-heading\">CATEGORY</h1>\n        <ul class=\"category-list\">\n");
#nullable restore
#line 11 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
             foreach (var item in ViewBag.genres)
            {

                

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                 if (ViewBag.genre == item.GenreId)
                {


#line default
#line hidden
#nullable disable
            WriteLiteral("                    <li class=\"category-item show-color\"><a");
            BeginWriteAttribute("href", " href=\"", 428, "\"", 467, 2);
            WriteAttributeValue("", 435, "/Home/Search?genre=", 435, 19, true);
#nullable restore
#line 17 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
WriteAttributeValue("", 454, item.GenreId, 454, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"category-link show-link\">");
#nullable restore
#line 17 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                                                                                                                               Write(item.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></li>\n");
#nullable restore
#line 18 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <li class=\"category-item\"><a");
            BeginWriteAttribute("href", " href=\"", 633, "\"", 672, 2);
            WriteAttributeValue("", 640, "/Home/Search?genre=", 640, 19, true);
#nullable restore
#line 21 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
WriteAttributeValue("", 659, item.GenreId, 659, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"category-link\">");
#nullable restore
#line 21 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                                                                                                          Write(item.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></li>\n");
#nullable restore
#line 22 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"

                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                 
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </ul>\n    </div>\n    <div class=\"container-right\">\n");
#nullable restore
#line 28 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
         foreach (Movie item in ViewBag.movies)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"movie-card\">\n            <div class=\"movie-image\">\n                <img");
            BeginWriteAttribute("src", " src=\"", 964, "\"", 981, 1);
#nullable restore
#line 32 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
WriteAttributeValue("", 970, item.Image, 970, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("\n                     width=\"135px\" height=\"180px\"");
            BeginWriteAttribute("alt", " alt=\"", 1032, "\"", 1038, 0);
            EndWriteAttribute();
            WriteLiteral(">\n            </div>\n            <div class=\"movie-title\">\n                <h2 class=\"movie-name\">");
#nullable restore
#line 36 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                                  Write(item.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\n                <p class=\"movie-srg\">Year : ");
#nullable restore
#line 37 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                                       Write(item.Year);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\n                <p class=\"movie-srg\">Genere: ");
#nullable restore
#line 38 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                                        Write(item.Genre.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\n");
#nullable restore
#line 39 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                  
                using(var db = new CenimaDBContext())
                {
                var rating = db.Rates.Where(x => x.MovieId == item.MovieId);
                double? count = 0;
                double s = 0;
                foreach (var a in rating)
                {
                count += a.NumericRating;
                s++;
                }
                var result = count / s;



#line default
#line hidden
#nullable disable
            WriteLiteral("                <p class=\"movie-srg\">Score : ");
#nullable restore
#line 53 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                                        Write(result);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\n");
#nullable restore
#line 54 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
                }
                

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\n            <div class=\"movie-action\">\n                <a");
            BeginWriteAttribute("href", " href=\"", 1871, "\"", 1908, 2);
            WriteAttributeValue("", 1878, "/Movie/Detail?id=", 1878, 17, true);
#nullable restore
#line 58 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
WriteAttributeValue("", 1895, item.MovieId, 1895, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"movie-rate\">Danh gia</a>\n            </div>\n        </div>\n");
#nullable restore
#line 61 "C:\Users\soncr\Desktop\BachHD_HE151355_Cinema-main\Cinema\Views\Home\Search.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\n</div>\n\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
