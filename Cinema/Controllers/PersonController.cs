﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cinema.Models;


namespace Cinema.Controllers
{
    public class PersonController : Controller
    {
        private readonly CenimaDBContext _context;

        public PersonController(CenimaDBContext context)
        {
            _context = context;
        }

        public IActionResult List()
        {
            var persons = _context.Persons.ToList();
            ViewBag.persons = persons;
            return View();
        }

      

       
    }
}
