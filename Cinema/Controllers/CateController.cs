﻿using Cinema.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;

namespace Cinema.Controllers
{
    public class CateController : Controller
    {
        private readonly ILogger<CateController> _logger;
        private readonly CenimaDBContext _context;

        public CateController(ILogger<CateController> logger, CenimaDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Update(int id, string descript)
        {
            Console.WriteLine(id + "    " + descript);
            var genre = _context.Genres.FirstOrDefault(x => x.GenreId == id);

            if (genre != null)
            {
                genre.Description = descript;

                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }


            return RedirectToAction("CateList", "Movie");
        }

        public IActionResult Delete(int id)
        {
            var genre = _context.Genres.FirstOrDefault(x => x.GenreId == id);
            var movie = _context.Movies.FirstOrDefault(x => x.GenreId == id);

            if (genre.GenreId !=  movie.Genre.GenreId)
            {
                _context.Genres.Remove(genre);
                _context.SaveChanges();
            }

            return RedirectToAction("CateList", "Movie");
        }
    }
    }
